#!/bin/sh

./node_modules/uglify-js/bin/uglifyjs \
  lib/maple/lib/bison.js \
  lib/maple/lib/Class.js \
  lib/maple/lib/Twist.js \
  lib/maple/Maple.js \
  lib/maple/Client.js \
  common/init.js \
  data/ship-stats.js \
  data/enemy-stats.js \
  common/util.js \
  common/map.js \
  common/weapon.js \
  common/ship.js \
  client/weapon.js \
  client/ship.js \
  client/screen.js \
  client/ui.js \
  client/main.js \
  -c \
  -m \
  --screw-ie8 \
  -o client/client-compiled.js

