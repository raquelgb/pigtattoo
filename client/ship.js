PigTattoo.Client.Ship = function() {
  var commonShip = new PigTattoo.Common.Ship();
  var commonShip_setup  = commonShip.setup;
  var commonShip_update = commonShip.update;
  var coords, destination, bearing, speed, minSpeed, range, target, targetCoords, orders, optimalRange, maneuverability, primaryWeapon; // Some properties I reuse
  var v, angleTo, angleDiff, nextCoords = [-1, -1], nextBearing, nextDestination; // And some I dont
  var imgObj;
  var pix;
  var animIndex, animLength;

  var getPix = function() { return pix; }

  var setup = function(_id, _config) {
    var _self = this;
    commonShip_setup(_id, _config);
    primaryWeapon = new PigTattoo.Client.Weapon();
    primaryWeapon.setup(_config.weapon, _self);
    commonShip.setPrimaryWeapon(primaryWeapon);
    imgObj = new Image();
    imgObj.onload = function() {
      pix = new Kinetic.Sprite({
              x: 0,
              y: 0,
              image: imgObj,
              offset: _config.pix_offset,
              animations: _config.pix_animations,
              animation: 'base',
              index: 0,
            });
      pix.on("mouseup", function(e) {
        if(_self == PigTattoo.Client.myShip) {
          PigTattoo.Client.myShip.setOrders(commonShip.ORDERS.move);
          PigTattoo.Client.myShip.setDestination(PigTattoo.Client.myShip.getCoords());
        } else if( PigTattoo.Client.enemyShips[_self.getId()] ) {
          PigTattoo.Client.myShip.setOrders(commonShip.ORDERS.kill);
          PigTattoo.Client.myShip.setTarget(_self);
        } else {
          PigTattoo.Client.myShip.setOrders(commonShip.ORDERS.protect);
          PigTattoo.Client.myShip.setTarget(_self);
        }
        PigTattoo.Client.client.send('move', [ PigTattoo.Client.myShip.getPosition() ]);
        e.cancelBubble = true;
      });
      PigTattoo.Client.Screen.getLayer(_config.pix_layer).add(pix);
      PigTattoo.Client.Screen.getLayer(_config.pix_layer).draw();
      animLength = pix.getAnimations().base.length;
      animIndex = 0;
    }
    imgObj.src=_config.pix_src;
  }

  var update = function(t) {
    commonShip_update(t); // This updates coords and bearing

    // Now try to guess where I'll be next to help render()
    coords = commonShip.getCoords();
    target = commonShip.getTarget();
    if(target) targetCoords = target.getCoords();
    orders = commonShip.getOrders();
    bearing = commonShip.getBearing();
    destination = commonShip.getDestination();
    speed = commonShip.getSpeed();
    minSpeed = commonShip.getMinSpeed();
    range = commonShip.getRange();
    optimalRange = commonShip.getOptimalRange();
    maneuverability = commonShip.getManeuverability();
    //primaryWeapon = commonShip.getPrimaryWeapon();

    nextDestination = destination;
    if( orders == commonShip.ORDERS.kill || orders == commonShip.ORDERS.chase || orders == commonShip.ORDERS.protect ) {
      if(util.distance(coords, targetCoords) > optimalRange)
        nextDestination = targetCoords;
      else
        nextDestination = coords;
    }

    v = [ nextDestination[0] - coords[0], nextDestination[1] - coords[1]];
    if( v[0] != 0 || v[1] != 0 ) { // I'm on the move
      angleTo = Math.atan2( v[1], v[0] );
      angleDiff = angleTo - bearing;
      nextBearing = bearing;
      if( Math.abs(angleDiff) > 0.000000001 * Math.PI ) {
        angleDiff += (angleDiff > Math.PI) ? -2 * Math.PI : (angleDiff < -Math.PI) ? 2 * Math.PI : 0

        if( Math.abs(angleDiff) < maneuverability ) nextBearing = angleTo;
        else
          nextBearing += (angleDiff > 0 ? 1 : -1 ) * maneuverability;
        nextBearing += (nextBearing > Math.PI) ? -2 * Math.PI : (nextBearing < -Math.PI) ? 2 * Math.PI : 0
      }

      spd = [ speed * Math.cos(bearing), speed * Math.sin(bearing) ];
      nextCoords[0] = Math.abs(v[0]) < Math.abs(spd[0])
                       ? nextDestination[0]
                       : coords[0] + spd[0];
      nextCoords[1] = Math.abs(v[1]) < Math.abs(spd[1])
                       ? nextDestination[1]
                       : coords[1] + spd[1];
    } else {
      if(target) { // Not moving, but may need to change bearing
        angleTo = Math.atan2( targetCoords[1] - coords[1], targetCoords[0] - coords[0] );
        angleDiff = angleTo - bearing;
        nextBearing = bearing;
        if( Math.abs(angleDiff) > 0.000000001 * Math.PI ) {
          angleDiff += (angleDiff > Math.PI) ? -2 * Math.PI : (angleDiff < -Math.PI) ? 2 * Math.PI : 0
          if( Math.abs(angleDiff) < maneuverability ) nextBearing = angleTo;
          else
            nextBearing += (angleDiff > 0 ? 1 : -1 ) * maneuverability;
          nextBearing += (nextBearing > Math.PI) ? -2 * Math.PI : (nextBearing < -Math.PI) ? 2 * Math.PI : 0
        }
      } else { // Not changing bearing
        nextBearing = bearing;
      }
    }

    if(minSpeed > 0.0) { // Drift if I need to
      spd = [ minSpeed * Math.cos(bearing), minSpeed * Math.sin(bearing) ];
      nextCoords = [coords[0] + spd[0], coords[1] + spd[1]];
    } else {
      nextCoords = coords;
    }
  }

  var render = function(t,u) {
    if(pix === undefined || nextCoords[0] < 0) return;
    if(++animIndex >= animLength) animIndex=0;
    pix.setIndex(animIndex);
    if(commonShip.doesRotate()) pix.setRotation(bearing + u * (nextBearing - bearing));
    pix.setX( coords[0] + u * (nextCoords[0] - coords[0]) );
    pix.setY( coords[1] + u * (nextCoords[1] - coords[1]) );
    if(primaryWeapon) primaryWeapon.render(t);
  }

  /* Add new exports */
  commonShip.setup = setup;
  commonShip.update = update;
  commonShip.render = render;
  commonShip.getPix = getPix;

  return commonShip;
};

