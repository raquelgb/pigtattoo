PigTattoo.Client.ufps = {};
PigTattoo.Client.ufps.frames = 0;
PigTattoo.Client.ufps.new_t = 0;
PigTattoo.Client.ufps.last_t = 0;
PigTattoo.Client.ufps.sum_t = 0;

PigTattoo.Client.rfps = {};
PigTattoo.Client.rfps.frames = 0;
PigTattoo.Client.rfps.new_t = 0;
PigTattoo.Client.rfps.last_t = 0;
PigTattoo.Client.rfps.sum_t = 0;

PigTattoo.Client.Main = Class(function() {
    Maple.Client(this, 30, 60);
  }, Maple.Client, {
    started: function() {
      PigTattoo.Client.myRoute = {};
      PigTattoo.Client.Screen.setup();
      PigTattoo.Client.myShip = new PigTattoo.Client.Ship();
      PigTattoo.Client.myShip.setup(0, PigTattoo.Data.SHIPSTATS.scout);
      PigTattoo.Client.myId = 0; // waiting for id from server
      PigTattoo.Client.allyShips = {};
      PigTattoo.Client.enemyShips = {};
      PigTattoo.Client.allTargets = {};

      PigTattoo.Client.UI.setup();
      this.log('Client started');
    },

    update: function(t, tick) {
      //PigTattoo.Client.ufps.new_t = Date.now();
      PigTattoo.Client.ufps.new_t = t;
      PigTattoo.Client.ufps.sum_t += (PigTattoo.Client.ufps.new_t-PigTattoo.Client.ufps.last_t);
      PigTattoo.Client.ufps.last_t = PigTattoo.Client.ufps.new_t;
      if(PigTattoo.Client.ufps.frames++ >= 100) {
        $('#ufps').html( (PigTattoo.Client.ufps.frames * 1000 / PigTattoo.Client.ufps.sum_t).toFixed(2) );
        PigTattoo.Client.ufps.frames=PigTattoo.Client.ufps.sum_t=0;
      }
      PigTattoo.Client.myShip.update(t);
      for(_id in PigTattoo.Client.allyShips) PigTattoo.Client.allyShips[_id].update(t);
      for(_id in PigTattoo.Client.enemyShips) PigTattoo.Client.enemyShips[_id].update(t);
      PigTattoo.Client.Screen.update(t);
    },

    render: function(t, tick, dt, u) {
      //PigTattoo.Client.rfps.new_t = Date.now();
      PigTattoo.Client.rfps.new_t = t;
      PigTattoo.Client.rfps.sum_t += (PigTattoo.Client.rfps.new_t-PigTattoo.Client.rfps.last_t);
      PigTattoo.Client.rfps.last_t = PigTattoo.Client.rfps.new_t;
      if(PigTattoo.Client.rfps.frames++ >= 100) {
        $('#rfps').html( (PigTattoo.Client.rfps.frames * 1000 / PigTattoo.Client.rfps.sum_t).toFixed(2) );
        PigTattoo.Client.rfps.frames=PigTattoo.Client.rfps.sum_t=0;
      }
      if(PigTattoo.Client.myShip === undefined) return; // Not ready yet
      PigTattoo.Client.myShip.render(t,u);
      PigTattoo.Client.UI.render(t,u);
      for(_id in PigTattoo.Client.allyShips) PigTattoo.Client.allyShips[_id].render(t,u);
      for(_id in PigTattoo.Client.enemyShips) PigTattoo.Client.enemyShips[_id].render(t,u);
      PigTattoo.Client.Screen.render(t,u);
    },

    stopped: function() {
     this.log('Stopped');
    },

    connected: function() {
      this.log('Connection established');
    },

    message: function(type, tick, data) {
      this.log('Message received:', type, data);
    },

    syncedMessage: function(type, tick, data) {
      if (type == 'connected') {
        for (_id in data[0]) {
          PigTattoo.Client.myId = _id;
          PigTattoo.Client.myShip.setId(_id);
          PigTattoo.Client.myShip.setPosition(data[0][_id]);
          PigTattoo.Client.allTargets[_id] = PigTattoo.Client.myShip;
          break;
        }
      } else if(type == 'sync') {
        for (_id in data[0]) {
          if (_id == PigTattoo.Client.myId) PigTattoo.Client.myShip.setPosition(data[0][_id]);
          else {
            if (PigTattoo.Client.allyShips[_id] == undefined) {
              PigTattoo.Client.allyShips[_id] = new PigTattoo.Client.Ship();
              PigTattoo.Client.allyShips[_id].setup(_id, PigTattoo.Data.SHIPSTATS[data[0][_id].model]);
              PigTattoo.Client.allTargets[_id] = PigTattoo.Client.allyShips[_id];
            }
            PigTattoo.Client.allyShips[_id].setPosition(data[0][_id]);
          }
        }
        for (_id in data[1]) {
          if (PigTattoo.Client.enemyShips[_id] == undefined) {
            PigTattoo.Client.enemyShips[_id] = new PigTattoo.Client.Ship();
            PigTattoo.Client.enemyShips[_id].setup(_id, PigTattoo.Data.ENEMYSTATS[data[1][_id].model]);
            PigTattoo.Client.allTargets[_id] = PigTattoo.Client.enemyShips[_id];
          }
          PigTattoo.Client.enemyShips[_id].setPosition(data[1][_id]);
        }
     }
     this.log('Synced message received:', type, data);
    },

    closed: function(byRemote, errorCode) {
      this.log('Connection closed:', byRemote, errorCode);
    },
});

PigTattoo.Client.client = new PigTattoo.Client.Main();
PigTattoo.Client.client.connect('localhost', 4000);

