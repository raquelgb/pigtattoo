PigTattoo.Client.Weapon = function() {
  var commonWeapon = new PigTattoo.Common.Weapon();
  var commonWeapon_setup  = commonWeapon.setup;
  var commonWeapon_update = commonWeapon.update;
  var firing, coords, targetCoords; // Some properties I reuse
  var lastFireT; // And some I don't
  var line;

  var setup = function(_config, _owner) {
    commonWeapon_setup(_config, _owner);
    line = new Kinetic.Line({ points: [0, 0, 2000, 2000], stroke: 'green', strokeWidth: 2, });
    PigTattoo.Client.Screen.getLayer('bullets').add(line);
    line.hide();
  }

  var update = function(t) {
    commonWeapon_update(t);
    firing = commonWeapon.isFiring();
    coords = commonWeapon.getCoords();
    targetCoords = commonWeapon.getTargetCoords();
    if(firing) {
      line.setPoints(coords.concat(targetCoords));
      lastFireT=t;
    }
  }

  var render = function(t,u) {
    if(firing) {
      line.show();
    }else if( (lastFireT - t) > 10 ) {
      line.hide();
    }
  }

  /* Add new exports */
  commonWeapon.setup = setup;
  commonWeapon.update = update;
  commonWeapon.render = render;

  return commonWeapon;
};

