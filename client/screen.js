PigTattoo.Client.Screen = (function() {
  var stage, bgLayers, layers, backgrounds;

  // Zoom level and limits
  var zoom         = 1.0;
  var nextZoom     = 1.0;
  var maxZoom      = 1.0;
  var minZoom      = 0.2;
  var zoomDuration = 0.4;
  var animating    = false;

  // Screen panning and limits
  var offset      = [0, 0];
  var nextOffset  = [0, 0];
  var minX = minY = PigTattoo.Common.Map.getMinCoord();
  var maxX = maxY = PigTattoo.Common.Map.getMaxCoord();

  // Some FPS optimization
  var _renderCount = 0;
  var _renderQuality = 1; // 1 = Full Quality .. 100 = Crappy scrolling

  var getStage = function() { return stage; }
  var getLayer = function(_layer) { return layers[_layer]; }
  var getOffset = function() { return offset; }
  var getZoom = function() { return zoom; }

  var setup = function() {
    stage = new Kinetic.Stage({
      container: 'gameCanvasWrap',
	    width: 580,
	    height: 202,
    });

    // Background-only, private layers
    bgLayers = {};
    bgLayers.bg      = new Kinetic.Layer();
    bgLayers.stars   = new Kinetic.Layer();
    bgLayers.clouds  = new Kinetic.Layer();

    // Layers that actually contains objects
    layers = {};
    layers.planets = new Kinetic.Layer();
    layers.enemies = new Kinetic.Layer();
    layers.ships   = new Kinetic.Layer();
    layers.bullets = new Kinetic.Layer();
    layers.hud     = new Kinetic.Layer();

    // Order is important here!
    stage.add(bgLayers.bg);
    stage.add(bgLayers.stars);
    stage.add(layers.planets);
    stage.add(layers.enemies);
    stage.add(layers.ships);
    stage.add(layers.bullets);
    stage.add(bgLayers.clouds);
    stage.add(layers.hud);

    // Set backgrounds
    backgrounds = {};
    setLayerBackground('bg',     'img/Backgrounds/starfield2.png', 0.1);
    setLayerBackground('stars',  'img/Backgrounds/starfield.png',  0.3); bgLayers.stars.setListening(false);
    setLayerBackground('clouds', 'img/Backgrounds/over.png',       1.5); bgLayers.clouds.setListening(false);

    $(window).resize( PigTattoo.Client.Screen.resize );
    $(document).ready( PigTattoo.Client.Screen.resize() );

    PigTattoo.Client.myRoute.line = new Kinetic.Line({ points: [0, 0, 2000, 2000], stroke: 'green', strokeWidth: 2, dashArray: [33, 10], });
    layers.ships.add(PigTattoo.Client.myRoute.line); // Add this before myShip, so it gets drawn behind
    PigTattoo.Client.myRoute.circle = new Kinetic.Circle({ x: 1000, y: 1000, stroke: 'green', strokeWidth: 2, radius: 4, });
    layers.ships.add(PigTattoo.Client.myRoute.circle);
  }

  var setLayerBackground = function(_layerName, _imgPath, _parallax) {
    var _layer = bgLayers[_layerName];
    var _layerImg = new Image();

    _layerImg.onload = function() {
      backgrounds[_layerName] = {};
      backgrounds[_layerName].parallax = _parallax;
      backgrounds[_layerName].width    = _layerImg.width;
      backgrounds[_layerName].height   = _layerImg.height;
      backgrounds[_layerName].img00    = new Kinetic.Image({ image: _layerImg, x: 0,               y: 0, });
      backgrounds[_layerName].img01    = new Kinetic.Image({ image: _layerImg, x: 0,               y: _layerImg.height, });
      backgrounds[_layerName].img10    = new Kinetic.Image({ image: _layerImg, x: _layerImg.width, y: 0, });
      backgrounds[_layerName].img11    = new Kinetic.Image({ image: _layerImg, x: _layerImg.width, y: _layerImg.height, });
      _layer.add(backgrounds[_layerName].img00);
      _layer.add(backgrounds[_layerName].img01);
      _layer.add(backgrounds[_layerName].img10);
      _layer.add(backgrounds[_layerName].img11);
      _layer.draw();
    }
    _layerImg.src=_imgPath;
  }

  var resize = function() {
    stage.setWidth($(window).width() - $('#gameSidebar').width());
    stage.setHeight($(window).height() - 5);
  }

  var setZoom = function(_zoom) {
    if(!_zoom) return;
    if(_zoom > maxZoom) _zoom = maxZoom;
    if(_zoom < minZoom) _zoom = minZoom;
    if(zoom != _zoom) {
      nextZoom = _zoom;
      nextOffset = centerOn(PigTattoo.Client.myShip.getCoords());
      animating = true;
    }
  }

  var centerOn = function(_coords) {
    var _offset = [0, 0];
    _offset[0] = -(_coords[0] - stage.getWidth() / (2 * nextZoom));
    _offset[1] = -(_coords[1] - stage.getHeight() / (2 * nextZoom));
    if(_offset[0] > -minX) _offset[0] = minX;
    if(_offset[0] < -maxX) _offset[0] = maxX;
    if(_offset[1] > -minY) _offset[1] = minY;
    if(_offset[1] < -maxY) _offset[1] = maxY;
    return _offset;
  }

  var update = function(t) {
    offset = nextOffset;
    zoom = nextZoom;
  }

  var render = function(t,u) {
    if(stage === undefined || layers === undefined) return;
    var mapCoords = PigTattoo.Client.myShip.getCoords();
    var screenCoords = [ (mapCoords[0] + offset[0]) * zoom, (mapCoords[1] + offset[1]) * zoom]; // not used?
    if(animating) {
      // TODO: really animate
      for(_layer in layers) {
        layers[_layer].setX(nextOffset[0]);
        layers[_layer].setY(nextOffset[1]);
        layers[_layer].setScale(nextZoom);
        layers[_layer].draw();
      }
      for(_layer in bgLayers) {
        // For now we do not zoom bg layers
        bgLayers[_layer].draw();
      }
      $('#zoomLevel').text(zoom.toFixed(2));
      animating = false;
    } else {
      nextOffset = centerOn(mapCoords);
      if((nextOffset[0] != offset[0] || nextOffset[1] != offset[1]) && (++_renderCount % _renderQuality == 0)) {
        _renderCount=0;
        for(_layer in layers) {
          layers[_layer].setX(nextOffset[0]);
          layers[_layer].setY(nextOffset[1]);
        }
        for(_layer in bgLayers) {
          if(backgrounds[_layer]) {
            _w = backgrounds[_layer].width;
            _h = backgrounds[_layer].height;
            _p = backgrounds[_layer].parallax;
            backgrounds[_layer].img00.setX( _p * (nextOffset[0] % _w) );
            backgrounds[_layer].img01.setX( _p * (nextOffset[0] % _w) );
            backgrounds[_layer].img10.setX( _p * (nextOffset[0] % _w) + _w );
            backgrounds[_layer].img11.setX( _p * (nextOffset[0] % _w) + _w );
            backgrounds[_layer].img00.setY( _p * (nextOffset[1] % _h) );
            backgrounds[_layer].img01.setY( _p * (nextOffset[1] % _h) + _h );
            backgrounds[_layer].img10.setY( _p * (nextOffset[1] % _h) );
            backgrounds[_layer].img11.setY( _p * (nextOffset[1] % _h) + _h );
            bgLayers[_layer].draw();
          }
        }
      }
      for(_layer in layers)
        layers[_layer].draw();
    }
  }

  return {
    getStage:     getStage,
    getLayer:     getLayer, // <= NOTICE: This only returns object layers, not background ones
    setup:        setup,
    resize:       resize,
    getOffset:    getOffset,
    getZoom:      getZoom,
    setZoom:      setZoom,
    update:       update,
    render:       render,
  }
})();

