PigTattoo.Client.UI = (function() {
  var screenOffset, screenZoom, coords, destination, orders, uiColor;

  var ordersColor = function(_orders) {
    if(_orders == PigTattoo.Client.myShip.ORDERS.move || _orders == PigTattoo.Client.myShip.ORDERS.chase)
      return 'green';
    if(_orders == PigTattoo.Client.myShip.ORDERS.wander || _orders == PigTattoo.Client.myShip.ORDERS.protect)
      return 'yellow';
    if(_orders == PigTattoo.Client.myShip.ORDERS.kill)
      return 'red';
    else
      return 'blue';
  }

  var setup = function() {
    PigTattoo.Client.Screen.getStage().on("mouseup", function(e){
      screenOffset = PigTattoo.Client.Screen.getOffset();
      destination = [(e.clientX / PigTattoo.Client.Screen.getZoom()) - screenOffset[0], (e.clientY / PigTattoo.Client.Screen.getZoom()) - screenOffset[1]];
      PigTattoo.Client.myShip.setDestination(destination);
      PigTattoo.Client.myShip.setOrders(PigTattoo.Client.myShip.ORDERS.move);
      PigTattoo.Client.client.send('move', [ PigTattoo.Client.myShip.getPosition() ]);
    });
    PigTattoo.Client.Screen.getStage().getContent().addEventListener("mousewheel", function(e){
      screenZoom = PigTattoo.Client.Screen.getZoom();
      PigTattoo.Client.Screen.setZoom( screenZoom * (e.wheelDeltaY > 0 ? 1.2 : 1/1.2 ) );
    }, false);
  }

  var render = function(t,u) {
    coords = PigTattoo.Client.myShip.getCoords();
    destination = PigTattoo.Client.myShip.getDestination();
    orders = PigTattoo.Client.myShip.getOrders();
    uiColor = ordersColor(orders);

    PigTattoo.Client.myRoute.line.setPoints(coords.concat(destination));
    PigTattoo.Client.myRoute.line.setStroke(uiColor);

    PigTattoo.Client.myRoute.circle.setX(destination[0]);
    PigTattoo.Client.myRoute.circle.setY(destination[1]);
    PigTattoo.Client.myRoute.circle.setStroke(uiColor);
  }

  return {
    setup: setup,
    render: render,
  }
})();
