var PigTattoo = require('../common/init');
PigTattoo.Common.Ship = require('../common/ship');
PigTattoo.Common.Util = require('../common/util');

PigTattoo.Server.Ship = function() {
  var commonShip = new PigTattoo.Common.Ship();
  var commonShip_setup = commonShip.setup;

  var setup = function(_id, _config) { // Call the standard primary weapon setup
    commonShip_setup(_id, _config);
    commonShip.setupPrimaryWeapon(_config.weapon);
  }

  commonShip.setClientPosition = function(_position) { // TODO: maybe return whether goords are too apart
    var _target = PigTattoo.Server.allTargets[_position.target];
    if( PigTattoo.Common.Util.distance( _position.coords, this.getCoords() ) < 300 * this.getSpeed() ) this.setCoords(_position.coords);
    this.setDestination(_position.destination);
    this.setBearing(_position.bearing);
    this.setOrders(_position.orders);
    this.setTarget(_target);
  }

  return commonShip;
}

module.exports = PigTattoo.Server.Ship;

