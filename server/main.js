var PigTattoo = require('../common/init');
var ObjectList = require('../lib/maple/lib/ObjectList.js');
var Maple = require('../lib/maple/Maple');
PigTattoo.Server.Ship = require('./ship');
PigTattoo.Common.Map = require('../common/map');
PigTattoo.Data.ENEMYSTATS = require('../data/enemy-stats');
PigTattoo.Data.SHIPSTATS = require('../data/ship-stats');
PigTattoo.Server.clientShips = {};
PigTattoo.Server.enemyShips = {};
PigTattoo.Server.allTargets = {};
PigTattoo.Server.client_ol = {}; // Work around maple's own ObjectList()

// PigTattoo -----------------------------------------------------------------------
PigTattoo.Server.Main = Maple.Class(function(clientClass) {
  Maple.Server(this, clientClass, [
        /* MSG        Direction  Content */
        'connected',  // S->C :: [ client.id ]
        'sync',       // S->C :: [ { clientShips.getPosition() }, { enemyShips.getPosition() } ]
        'move',       // C->S :: [ client.getPosition() ]
        ]);

}, Maple.Server, {

  started: function() {
    this.log('Started');
    enemyShips = PigTattoo.Server.enemyShips;
    allTargets = PigTattoo.Server.allTargets;
    PigTattoo.Server.mapWidth = PigTattoo.Common.Map.getMaxCoord() - PigTattoo.Common.Map.getMinCoord();
    PigTattoo.Server.mapHeight = PigTattoo.Common.Map.getMaxCoord() - PigTattoo.Common.Map.getMinCoord();
    for (_id=0; _id<10; _id++) {
      x = Math.floor(Math.random()*601);
      y = Math.floor(Math.random()*601);
      enemyShips[_id] = new PigTattoo.Server.Ship();
      enemyShips[_id].setCoords([x, y]);
      enemyShips[_id].setDestination([x,y]);
      enemyShips[_id].setup(_id, PigTattoo.Data.ENEMYSTATS.ufo);
      allTargets[_id] = enemyShips[_id];
    }
  },

  update: function(t, tick) {
    allyData = {};
    enemyData = {};
    enemyShips = PigTattoo.Server.enemyShips;
    this._clients.each(function(client) {
      ship = clientShips[client.id];
      ship.update(t);
      if (tick % 50 === 0) allyData[client.id] = ship.getPosition();
    }, this);
    for (_id in enemyShips) {
      enemyShips[_id].update(t);
      if (tick % 50 === 0) {
        enemyShips[_id].setDestination([ Math.random() * PigTattoo.Server.mapWidth, Math.random() * PigTattoo.Server.mapHeight]);
        enemyData[enemyShips[_id].getId()] = enemyShips[_id].getPosition();
      }
    }
    if (tick % 50 === 0) {
      this.broadcast('sync', [allyData, enemyData]);
    }
  },

  stopped: function() {
    this.log('Stopped');
  },

  connected: function(client) {
    clientShips = PigTattoo.Server.clientShips;
    allTargets = PigTattoo.Server.allTargets;
    client_ol = PigTattoo.Server.client_ol;
    x = Math.floor(Math.random()*601);
    y = Math.floor(Math.random()*601);
    clientShips[client.id] = new PigTattoo.Server.Ship();
    clientShips[client.id].setCoords([x, y]);
    clientShips[client.id].setDestination([300,300]);
    clientShips[client.id].setup(client.id, PigTattoo.Data.SHIPSTATS.scout);
    allTargets[client.id] = clientShips[client.id];
    client_ol[client.id] = new ObjectList();
    client_ol[client.id].add(client);
    data = {};
    data[client.id] = clientShips[client.id].getPosition();
    this.broadcast('connected', [data], client_ol[client.id]);
    // maybe broadcast sync msg here
    this.log('Client has connected:', client.id, client.isBinary);
  },

  message: function(client, type, tick, data) {
    clientShips = PigTattoo.Server.clientShips;
    if(type == 'move' && clientShips[client.id] && data && data.length > 0) clientShips[client.id].setClientPosition(data[0]);
    this.log('New Message received:', client, type, tick, data);
  },

  requested: function(req, res) {
    this.log('HTTP Request');
  },

  disconnected: function(client) {
    clientShips = PigTattoo.Server.clientShips;
    allTargets = PigTattoo.Server.allTargets;
    delete clientShips[client.id];
    delete allTargets[client.id];
    this.log('Client has disconnected:', client.id);
  }

});


PigTattoo.Server.Client = Maple.Class(function(server, conn, isBinary) {
  Maple.ServerClient(this, server, conn, isBinary );

}, Maple.ServerClient, {

  message: function(type, tick, data) {
    console.log('Client got message:', type, tick, data);
  }

});


PigTattoo.Server.server = new PigTattoo.Server.Main(PigTattoo.Server.Client);
PigTattoo.Server.server.start({
  port: 4000,
  logicRate: 1
});

