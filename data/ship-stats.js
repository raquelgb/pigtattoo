if(typeof window == "undefined") var PigTattoo = require('../common/init');
PigTattoo.Data.SHIPSTATS = {}

PigTattoo.Data.SHIPSTATS.scout = {
  model: 'scout',
  speed: 2.0,
  minSpeed: 0.5,
  range: 400.0,
  rotate: true,
  pix_src: 'img/Ship/Spritesheet_64x29.png',
  pix_animations: {
    base: [
      { x:0,
        y:0,
        width: 64,
        height: 29, },
      { x:0,
        y:29,
        width: 64,
        height: 29, },
      { x:0,
        y:58,
        width: 64,
        height: 29, },
      { x:0,
        y:87,
        width: 64,
        height: 29, },
    ]
  },
  pix_offset: [48, 14],
  pix_layer: 'ships',
  weapon: {
    range: 400.0,
    aperture: 0.25 * Math.PI,
    cooldown: 45,
    offset: [15, 0],
  },
};

if(typeof window == "undefined") module.exports = PigTattoo.Data.SHIPSTATS;

