if(typeof window == "undefined") var PigTattoo = require('../common/init');
PigTattoo.Data.ENEMYSTATS = {}

PigTattoo.Data.ENEMYSTATS.ufo = {
  model: 'ufo',
  speed: 1.6,
  minSpeed: 0,
  range: 400.0,
  rotate: false,
  pix_src: 'img/Enemy/eSpritesheet_40x30.png',
  pix_animations: {
    base: [
      { x:0,
        y:0,
        width: 40,
        height: 30, },
      { x:0,
        y:30,
        width: 40,
        height: 30, },
      { x:0,
        y:60,
        width: 40,
        height: 30, },
      { x:0,
        y:90,
        width: 40,
        height: 30, },
      { x:0,
        y:120,
        width: 40,
        height: 30, },
      { x:0,
        y:150,
        width: 40,
        height: 30, },
    ],
  },
  pix_offset: [20, 20],
  pix_layer: 'enemies',
  weapon: {
    range: 400.0,
    aperture: 0.25 * Math.PI,
    cooldown: 45,
  },
};

if(typeof window == "undefined") module.exports = PigTattoo.Data.ENEMYSTATS;

