if(typeof window == "undefined") var PigTattoo = require('./init');

PigTattoo.Common.Map = (function() {
  var minCoord = 0,
      maxCoord = 25000;

  var getMinCoord = function() { return minCoord; }
  var getMaxCoord = function() { return maxCoord; }

  return {
    getMinCoord: getMinCoord,
    getMaxCoord: getMaxCoord,
  }
})();

if(typeof window == "undefined") module.exports = PigTattoo.Common.Map;
