if(typeof window == "undefined") var PigTattoo = require('./init');

PigTattoo.Common.Util = (function() {
  var distance = function(_from, _to) {
    var _dx = _to[0] - _from[0];
    var _dy = _to[1] - _from[1];

    return Math.sqrt( _dx*_dx + _dy*_dy );
  }

  return {
    distance: distance,
  }
})();

if(typeof window == "undefined") module.exports = PigTattoo.Common.Util;
