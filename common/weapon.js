if(typeof window == "undefined") {
  var PigTattoo = require('./init');
  util = require('./util');
}
else {
  util = PigTattoo.Common.Util;
}

PigTattoo.Common.Weapon = function() {
  var _self = this;

  var firing;   // Am I firing during this update?
  var range;    // How far can I hit the target
  var aperture; // Angle at which I can shoot
  var cooldown; // How many updates between shots
  var timer;    // How many updates till ready to fire
  var owner;    // The ship mounting this weapon, needs getCoords(), getBearing() and getTarget()
  var offset = [0, 0];   // The weapon position relative to the ship mounting it, allows for satellite weapons
  var bearingOffset = 0; // The weapon bearing relative to the ship mounting it, allows for backwards firing weapons
  var coords, bearing, target, targetCoords; // Some stuff I get from my owner
  var v = [-1, -1];
  var angleTo, angleDiff;

  this.getTimer = function() { return timer; };
  this.getCooldown = function() { return cooldown; };
  this.isFiring = function() { return firing; };
  this.getCoords = function() { return (coords ? coords : undefined); };
  this.getTargetCoords = function() { return (target ? targetCoords : undefined); }

  var fire = function() {
    timer = cooldown;
    firing = true;
  }

  this.setup = function(_config, _owner) {
    range =  _config.range;
    aperture =  _config.aperture;
    cooldown =  _config.cooldown;
    if(_config.offset) offset =  _config.offset;
    if(_config.bearingOffset) bearingOffset =  _config.bearingOffset;
    owner = _owner;
    timer = cooldown; // Start loading!
    firing = false;
  }

  this.update = function(t) {
    firing = false;
    if(!timer || !(--timer)) { // Ready to fire!
      target = owner.getTarget();
      if(!target) return // TODO: make sure target DOES exist before shoooting!

      targetCoords = target.getCoords().slice();
      coords = owner.getCoords().slice();
      bearing = owner.getBearing(); // I use it as is for now, add offset later
      if(offset[0] != 0 || offset[1] != 0) {
        if(owner.doesRotate()) {
          coords[0] += ( offset[0] * Math.cos(bearing) + offset[1] * Math.sin(bearing) ) ;
          coords[1] += ( offset[1] * Math.cos(bearing) + offset[0] * Math.sin(bearing) ) ;
        } else {
          coords[0] += offset[0];
          coords[1] += offset[1];
        }
      }
      if(util.distance(targetCoords, coords) > range) return; // Target out of range

      bearing += bearingOffset;
      v = [ targetCoords[0] - coords[0], targetCoords[1] - coords[1] ];
      angleTo = Math.atan2( v[1], v[0] );
      angleDiff = angleTo - bearing;
      angleDiff += (angleDiff > Math.PI) ? -2 * Math.PI : (angleDiff < -Math.PI) ? 2 * Math.PI : 0;
      if(angleDiff > aperture) return; // Not aimed towards target

      fire();
    }
  }

};

if(typeof window == "undefined") module.exports = PigTattoo.Common.Weapon;
