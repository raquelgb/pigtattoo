if(typeof window == "undefined") {
  var PigTattoo = require('./init');
  PigTattoo.Common.Map = require('./map');
  PigTattoo.Common.Weapon = require('./weapon');
  util = require('./util');
} else {
  util = PigTattoo.Common.Util;
}

PigTattoo.Common.Ship = function() {
  var ORDERS = this.ORDERS = {
    move: 0,    // Try to reach destination, do not attack
    wander: 1,  // Try to reach destination, attack enemies
    kill: 2,    // Go within optimal range of target, attack target
    chase: 3,   // Go within chase range of target, do not attack
    protect: 4, // Go within protect range of target, attack enemies
  };

  var _self = this;

  var coords = [0, 0];       // Where I am
  var destination = [0, 0];  // Where I'm going
  var speed = 2.0;           // My raw maximum speed
  var minSpeed = 0.5;        // How much I drift when I don't want to move
  var v = [0, 0];            // My destination vector (destination-coords)
  var spd = [0, 0];          // What I'll actually move v ** speed
  var bearing = 0;           // Angle I'm facing, in radins from -PI..PI
  var angleTo, angleDiff;    // Some helpers
  var orders = ORDERS.move;  // The ship behavior
  var target = null;         // Who am I following / attacking, if any
  var targetCoords = [0, 0]; // Where is he / she
  var range = 300.0;         // How far can I shoot from
  var optimalRange = 150.0;  // How far do I prefer to shoot from
  var chaseRange =   200.0;  // How far do I prefer to chase someone to
  var protectRange = 200.0;  // How far do I prefer to stand when protecting
  var maneuverability = 0.01 * Math.PI; // How fast can I turn
  var rotate = true;         // Does the ship (and pix) rotate or not

  var id;
  var model;

  var mapWidth;
  var mapHeight;
  var primaryWeapon;

  this.getCoords = function() { return coords; }
  this.setCoords = function(_coords) { coords = _coords; }
  this.getDestination = function() { return destination; }
  this.getBearing = function() { return bearing; }
  this.setBearing = function(_bearing) { bearing = _bearing; }
  this.getSpeed = function() { return speed; }
  this.getMinSpeed = function() { return minSpeed; }
  this.getRange = function() { return range; }
  this.getOptimalRange = function() { return optimalRange; }
  this.getManeuverability = function() { return maneuverability; }
  this.getPrimaryWeapon = function() { return primaryWeapon; }
  this.setPrimaryWeapon = function(_primaryWeapon) { primaryWeapon = _primaryWeapon; }
  this.getOrders = function() { return orders; }
  this.setOrders = function(_orders) { orders = _orders; }
  this.getTarget = function() { return target; }
  this.setTarget = function(_target) { target = _target; }
  this.getId = function() { return id; }
  this.setId = function(_id) { id = _id; }
  this.getModel = function() { return model; }
  this.isMoving = function() { return v[0] != 0 || v[1] != 0; }
  this.doesRotate = function() { return rotate; }

  this.setupPrimaryWeapon = function(_config) {
    primaryWeapon = new PigTattoo.Common.Weapon();
    primaryWeapon.setup(_config, _self);
  };

  this.getPosition = function() {
    var _target = target ? target.getId() : undefined;
    return {
      model: model,
      coords: coords,
      destination: destination,
      bearing: bearing,
      orders: orders,
      target: _target,
    }
  }

  this.setPosition = function(_position) {
    var _target =  (typeof window == "undefined" ? PigTattoo.Server.allTargets : PigTattoo.Client.allTargets)[_position.target];
    _self.setCoords(_position.coords);
    _self.setDestination(_position.destination);
    _self.setBearing(_position.bearing);
    _self.setOrders(_position.orders);
    _self.setTarget(_target);
  }

  this.setup = function(_id, _config) {
    id = _id;
    model = _config.model;
    speed = _config.speed;
    minSpeed = _config.minSpeed;
    range = _config.range;
    optimalRange = _config.optimalRange || _config.range / 2 || 100.0;
    rotate = _config.rotate;
    mapWidth = PigTattoo.Common.Map.getMaxCoord() - PigTattoo.Common.Map.getMinCoord();
    mapHeight = PigTattoo.Common.Map.getMaxCoord() - PigTattoo.Common.Map.getMinCoord();
    /* primaryWeapon = new PigTattoo.Common.Weapon();
    primaryWeapon.setup(_config.weapon, _self);*/ // <= This now happens in this.setupPrimaryWeapon (or in this.setPrimaryWeapon)
  }

  this.setDestination = function(_destination) {
    if(_destination) {
      if(_destination[0] < PigTattoo.Common.Map.getMinCoord()) _destination[0] = PigTattoo.Common.Map.getMinCoord();
      if(_destination[0] > PigTattoo.Common.Map.getMaxCoord()) _destination[0] = PigTattoo.Common.Map.getMaxCoord();
      if(_destination[1] < PigTattoo.Common.Map.getMinCoord()) _destination[1] = PigTattoo.Common.Map.getMinCoord();
      if(_destination[1] > PigTattoo.Common.Map.getMaxCoord()) _destination[1] = PigTattoo.Common.Map.getMaxCoord();
    }
    destination = _destination;
  }

  this.update = function(t) {
    if(target) targetCoords = target.getCoords();
    if(!target && orders == ORDERS.kill) orders = ORDERS.wander;
    if(!target && orders == ORDERS.chase) orders = ORDERS.move;
    if(!target && orders == ORDERS.protect) orders = ORDERS.wander;

    if( orders == ORDERS.kill || orders == ORDERS.chase || orders == ORDERS.protect ) {
      if(util.distance(coords, targetCoords) > optimalRange)
        destination = targetCoords;
      else
        destination = coords;
    }

    v = [ destination[0] - coords[0], destination[1] - coords[1]];
    if( _self.isMoving() ) {
      angleTo = Math.atan2( v[1], v[0] );
      angleDiff = angleTo - bearing;
      if( Math.abs(angleDiff) > 0.000000001 * Math.PI ) {
        angleDiff += (angleDiff > Math.PI) ? -2 * Math.PI : (angleDiff < -Math.PI) ? 2 * Math.PI : 0

        if( Math.abs(angleDiff) < maneuverability ) bearing = angleTo;
        else
          bearing += (angleDiff > 0 ? 1 : -1 ) * maneuverability;
        bearing += (bearing > Math.PI) ? -2 * Math.PI : (bearing < -Math.PI) ? 2 * Math.PI : 0
      }

      spd = [ speed * Math.cos(bearing), speed * Math.sin(bearing) ];
      if(Math.abs(v[0]) < Math.abs(spd[0]) &&
         Math.abs(v[1]) < Math.abs(spd[1]) &&
         ((spd[0] > 0 && v[0] > 0) || (spd[0]<0 && v[0] < 0)) &&
         ((spd[1] > 0 && v[1] > 0) || (spd[1]<0 && v[1] < 0))
         )
        coords = destination;
      else
        coords = [coords[0] + spd[0], coords[1] + spd[1]];
    } else {
      if(target) { // Not moving, but may need to change bearing
        angleTo = Math.atan2( targetCoords[1] - coords[1], targetCoords[0] - coords[0] );
        angleDiff = angleTo - bearing;
        if( Math.abs(angleDiff) > 0.000000001 * Math.PI ) {
          angleDiff += (angleDiff > Math.PI) ? -2 * Math.PI : (angleDiff < -Math.PI) ? 2 * Math.PI : 0

          if( Math.abs(angleDiff) < maneuverability ) bearing = angleTo;
          else bearing += (angleDiff > 0 ? 1 : -1 ) * maneuverability;
          bearing += (bearing > Math.PI) ? -2 * Math.PI : (bearing < -Math.PI) ? 2 * Math.PI : 0
        }
      }

      if(minSpeed > 0.0) { // Drift if I need to
        spd = [ minSpeed * Math.cos(bearing), minSpeed * Math.sin(bearing) ];
        coords = [coords[0] + spd[0], coords[1] + spd[1]];
      }
    }

    if(primaryWeapon) primaryWeapon.update(t);
  }

};

if(typeof window == "undefined") module.exports = PigTattoo.Common.Ship;
